# Transmitter external module pinout

The transmitter (Taranis, Jumper, RadioMaster, etc) pinout is, from top to bottom:

* PPM
* +6V
* +BAT
* GND
* ANT

It's illustrated in this photo:

[![pinout.jpeg](/resources/72d23239af1541d4b170271c1e9e21eb.jpg)](/resources/72d23239af1541d4b170271c1e9e21eb.jpg)



* * *

<p style="font-size:80%; font-style: italic">
Last updated on November 24, 2020. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
