# FPV frequency chart

If you're curious about which frequencies to use for video when there are multiple people flying FPV, as well as which bands are used by various manufacturers, here's a handy chart:

[![FPV frequency chart](/resources/48cfa1097ca847c5a9ff8229005e7f50.png)](/resources/48cfa1097ca847c5a9ff8229005e7f50.png)

It was made by [5zero7 RC](https://youtu.be/wScS5XloviM) with information from [a Propwashed article](https://www.propwashed.com/video-frequency-management/).


## Best channel groups

Here are the best channel groups for each number of pilots, from the chart:

- 2 pilots - E2 (5685), E6 (5905)
- 3 pilots - E2 (5685), F4 (5800), E6(5905)
- 4 pilots - E2 (5685), F2 (5760), F7 (5860) E6 (5905)
- 5 pilots - E2 (5685), F2 (5760), F4 (5800), F7 (5860), E6 (5905)
- IMD5C - R1 (5658), R2 (5695), F2 (5760), F4 (5800), E5 (5885)
- 6 pilots - E2 (5685), F1 (5740), F3 (5780), F5 (5820), F7 (5860), E6 (5905)
- IMD6C - R1 (5658), R2 (5695), F2 (5760), F4 (5800), F8 (5880), R8 (5917)



* * *

<p style="font-size:80%; font-style: italic">
Last updated on January 09, 2021. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
