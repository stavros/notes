# Mini Drak build condensed instructions

Sources:

1.  [Håvard's Mini Drak build video](https://www.youtube.com/watch?v=L4_dJxiQUjs)
2.  [Team Legit's build video](https://www.youtube.com/watch?v=LMbbIzs1TOQ)

To start building:

1.  Nubs:
    1.  Cut off all the nubs with **a fresh blade**, and sand the remainder down with a sanding block to make it flush with the rest of the foam.
2.  Motor:
    1.  Cut the top left and right corner of the motor mount off, so it doesn't conflict with the screws in the foam.
    2.  Use **only a hacksaw** to open a slot in the foam for the motor mount to go through.
    3.  Insert the mount into the slot, test it out and remove it again.
    4.  Apply ample glue both inside the slot and on the motor mount, both on the sides and the top.
    5.  Insert the motor mount into the slot, put a hard surface (like a hardcover book) under the foam, another hard surface at the top, add some weight or clamp and leave to cure.
3.  Thick spars:
    1.  Remove the spars from the sleeves and keep the sleeves. **Do not cut the spars**.
    2.  Insert the sleeves in the wings and measure them out. Mark the point in which the sleeve sticks out of the wing and cut with a dremel **outdoors**.
    3.  Trim the sleeves for the main fuselage.
    4.  Use some sandpaper wrapped around the spar to lightly scuff the body where the spars will go.
    5.  Put the rear spar into the sleeve, to help you push the sleeve down into the foam. Make sure it's pushed down all the way to the bottom.
    6.  **Check that the wings are straight.**
    7.  CA glue the sleeve into the body while making sure it's flat and down all the way inside the body.
    8.  Fill the cavity with E6000 so it seeps around the edges to hold the entire sleeve. Use **lots** of glue.
    9.  Put tape over the edges so the tape doesn't spill over either edge.
    10.  Put the wings on something that keeps them level, so the glue doesn't flow downhill.
    11.  For the main body, tape the sides of the sleeve and the insides so the glue doesn't seep in.
    12.  Make sure the main body is level while the glue cures.
    13.  Add spar caps and glue with E6000.
4.  Thin spars:
    1.  Lightly open the channels with a hacksaw blade, so it sits exactly on the surface.
    2.  Cut the spars longer, so you can grip the end and spin it while gluing.
    3.  Tape around the channel so glue doesn't spill out over the channel.
    4.  Put glue in, turn the spar around and remove excess.
    5.  Take the tape off ten minutes after you apply the glue, so the tape doesn't stick.
5.  Elevons:
    1.  Mark the top and bottom on the balsa.
    2.  Cut the balsa in some way.
    3.  Hold both elevons together and sand them together to make them identical.
    4.  Possibly sand the balsa where it meets the body, for full articulation.
    5.  Mount the elevons with two pieces of tape stuck to each other, overlapping, so there are two sticky sides.
6.  Servos:
    1.  Possibly use larger control horns, found on Thingiverse, because reasons.
7.  Winglets:
    1.  Glue the winglets completely flush with the tip of the wing.

Notes:

> Make sure you use the tallest control horns possible and have loads of expo in for the roll axis (pitch is fine). Matt (because the MD is very roll sensitive as I found out to my surprise).

* * *

<p style="font-size:80%; font-style: italic">
Last updated on November 27, 2020. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
