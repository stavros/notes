# Info on various hardware components

I bought various components from AliExpress, and sometimes they lack info. I've added info about them here:

## USB-C decoy breakouts: [5V 9V 12V 15V 20V adjustable 3A PD/QC multi-protocol decoy fast charging boost module TYPE-C USB interface](https://www.aliexpress.com/item/1005006064674815.html)

Specification:
* Supports multiple fast charging protocols
* Maximum support 3A
* TYPE-C port power supply
* Support multiple fast charging protocols: PD3.0/2.0, QC3.0/2.0, FCP, AFC
* PD/QC multi-protocol decoy 5V, 9V, 12V, 15V, 20V voltage adjustable

The manufacturer has said that this is the truth table:

| 1 | 2 | 3 | Voltage |
|---|---|---|---------|
| L | L | H |       5 |
| L | L | L |       9 |
| L | H | L |      12 |
| H | H | L |      15 |
| H | L | L |      20 |

This basically seems to be the same truth table as the original [USB PD decoy](https://hackaday.io/project/187112-usb-pd-decoy), except the logic levels are inverted (high becomes low and vice-versa), and SW1 in the original is M3, SW2 is M1, and SW3 is M2. Pretty convoluted, but it works.

* * *

<p style="font-size:80%; font-style: italic">
Last updated on November 23, 2023. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
