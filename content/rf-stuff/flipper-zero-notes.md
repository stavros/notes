# Flipper Zero notes

# Useful links

* [CAME protocol brute forcer](https://github.com/BitcoinRaven/CAMEbruteforcer).
* [A more generic sub-Ghz brute forcer](https://github.com/tobiabocchi/flipperzero-bruteforce)
* [How the sub-Ghz remote app works](https://www.reddit.com/r/flipperzero/comments/zbiuwo/subghz_remote_map_configure_tutorial_for_new/?utm_source=share&utm_medium=ios_app&utm_name=iossmf)
* [How to store 2 to 3 different EM41xx on a single T5577](https://forum.dangerousthings.com/t/hack-store-2-to-3-different-em41xx-on-a-single-t5577/12116)
* [Universal Radio Hacker](https://github.com/jopohl/urh)
* [Flipper Maker](https://flippermaker.github.io/)
* [Awesome Flipper Zero](https://github.com/djsime1/awesome-flipperzero)
* [KeeLoq datasheet](http://ww1.microchip.com/downloads/en/devicedoc/21143b.pdf)

# Videos

* This EEVblog video has very useful information on how IR protocols work: [EEVblog #506 - IR Remote Control Arduino Protocol Tutorial](https://www.youtube.com/watch?v=BUvFGTxZBG8)
* Samy Kamkar's DEF CON 23 video [Drive it like you Hacked it](https://www.youtube.com/watch?v=UNgvShN4USU) is a great intro to how non-rolling-code garage openers (and other old bit-shift receivers) work.
* [Excellent deep-dive into rolling code 433 MHz protocols](https://www.youtube.com/watch?v=x4ml1JAH1q0&list=PLM1cyTMe-PYJfnlDk3NjM85kU5VyCViNp&index=2)
* [Lots of detail about KeeLoq, and some attacks](https://www.youtube.com/watch?v=Zr_NCoSH2cg).

* * *

<p style="font-size:80%; font-style: italic">
Last updated on May 21, 2024. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
